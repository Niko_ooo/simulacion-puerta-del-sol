﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour
{
    [SerializeField] private int randomVariable;

    double PI = 3.141592;


    // Start is called before the first frame update
    void Start()
    {
        calculateSomethingRandom();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void calculateSomethingRandom()
    {
        byte indexMax = 5;
        for(byte i = 0; i< indexMax; ++i)
        {
            Debug.Log("Hello");
        }
    }
}
